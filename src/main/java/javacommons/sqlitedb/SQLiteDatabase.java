package javacommons.sqlitedb;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import org.sqlite.SQLiteDataSource;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.sql.DataSource;

public class SQLiteDatabase {

    private SQLiteDataSource ds;

    private static ThreadLocal<Exception[]> ex = new ThreadLocal<Exception[]>() {
        @Override
        protected Exception[] initialValue() {
            return new Exception[1];
        }
    };

    public static Exception getException() {
        return ex.get()[0];
    }

    public static void setException(Exception exception) {
        ex.get()[0] = exception;
    }

    protected String prepareDbDirectory(String path) throws IOException {
        if (path == ":memory:") {
            return path;
        }
        File f = new File(path);
        f = f.getAbsoluteFile();
        String absolutePath = f.getCanonicalPath().replaceAll("\\\\", "/");
        f = f.getParentFile();
        f.mkdirs();
        return absolutePath;
    }

    public SQLiteDatabase(String path) throws Exception {
        path = this.prepareDbDirectory(path);
        this.ds = new SQLiteDataSource();
        this.ds.setUrl("jdbc:sqlite:" + path);
    }

    public SQLiteDatabase(String path, String sqlResourcePath) throws Exception {
        path = this.prepareDbDirectory(path);
        boolean dbExists = new File(path).exists();
        this.ds = new SQLiteDataSource();
        this.ds.setUrl("jdbc:sqlite:" + path);
        if (!dbExists) {
            this.executeUpdateFromResource(sqlResourcePath);
        }
    }

    public SQLiteDatabase(String path, File sqlFile) throws Exception {
        path = this.prepareDbDirectory(path);
        boolean dbExists = new File(path).exists();
        this.ds = new SQLiteDataSource();
        this.ds.setUrl("jdbc:sqlite:" + path);
        if (!dbExists) {
            this.executeUpdateFromFile(sqlFile);
        }
    }

    public static SQLiteDatabase create(String path) {
        SQLiteDatabase.setException(null);
        try {
            SQLiteDatabase db = new SQLiteDatabase(path);
            return db;
        } catch (Exception ex) {
            SQLiteDatabase.setException(ex);
            return null;
        }
    }

    public static SQLiteDatabase create(String path, String sqlResourcePath) {
        SQLiteDatabase.setException(null);
        try {
            SQLiteDatabase db = new SQLiteDatabase(path, sqlResourcePath);
            return db;
        } catch (Exception ex) {
            SQLiteDatabase.setException(ex);
            return null;
        }
    }

    public String getDriverName() {
        return "org.sqlite.JDBC";
    }

    public String getUrl() {
        return this.ds.getUrl();
    }

    public DataSource getDataSource() {
        return this.ds;
    }

    public Connection getConnection() throws SQLException {
        return this.ds.getConnection();
    }

    public void executeUpdate(String sql) throws SQLException {
        try (Connection conn = this.getConnection()) {
            try (Statement stmt = conn.createStatement()) {
                stmt.executeUpdate(sql);
            }
        }
    }

    public void executeUpdate(List<String> sqlList) throws SQLException {
        try (Connection conn = this.getConnection()) {
            try (Statement stmt = conn.createStatement()) {
                for (String sql : sqlList) {
                    stmt.executeUpdate(sql);
                }
            }
        }
    }

    static String substituteEnvVars(String text) {
        StringBuffer sb = new StringBuffer();
        String pattern = "\\$\\{([A-Za-z0-9_]+)(?::([^\\}]*))?\\}";
        Pattern expr = Pattern.compile(pattern);
        Matcher matcher = expr.matcher(text);
        while (matcher.find()) {
            final String varname = matcher.group(1);
            String envValue = System.getenv(varname);
            if (envValue == null) {
                envValue = matcher.group(2);
                if (envValue == null)
                    envValue = "";
            }
            matcher.appendReplacement(sb, envValue.replaceAll("\\\\", "\\\\\\\\"));
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    public void executeUpdateFromFile(File file) throws Exception {
        //System.out.println(file.toString());
        String path = file.toString();
        //System.out.println(path);
        path = substituteEnvVars(path);
        //System.out.println(path);
        path = path.replaceAll("\\\\", "/");
        //System.out.println(path);
        String content = Files.asCharSource(new File(path), Charsets.UTF_8).read();
        this.executeUpdate(content);
    }

    public void executeUpdateFromResource(String path) throws Exception {
        this.executeUpdate(Resources.asString(path));
    }

}
