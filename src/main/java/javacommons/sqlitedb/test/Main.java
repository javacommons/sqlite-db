package javacommons.sqlitedb.test;

import javacommons.sqlitedb.SQLiteDatabase;

import java.io.File;

public class Main {
    public static void main(String[] args) throws Exception {
        System.out.println("Main.main()!");
        new SQLiteDatabase(":memory:", new File("${HOME}/ddl/qiita.sql"));
    }
}
